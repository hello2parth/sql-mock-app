const path = require('path');

const reactLintConfigPath = path.resolve(__dirname, './eslint/configs/react.js');
const jsxA11yLintConfigPath = path.resolve(__dirname, './eslint/configs/jsx-a11y.js');

module.exports = {
  "parser": "@babel/eslint-parser",
  "extends": [
    "airbnb",
    reactLintConfigPath,
    jsxA11yLintConfigPath,
    "prettier",
    "react-app"
  ],
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  "globals": {
  },
  "plugins": ["prettier"],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    },
    "requireConfigFile": false
  },
  "rules": {
    "arrow-body-style": [2, "as-needed"],
    "class-methods-use-this": 0,
    "default-case": [2, { "commentPattern": "^skip\\sdefault" }],
    "import/imports-first": 0,
    "import/newline-after-import": 0,
    "import/no-dynamic-require": 0,
    "import/no-extraneous-dependencies": 0,
    "import/no-named-as-default": 0,
    "import/no-unresolved": 2,
    "import/no-webpack-loader-syntax": 0,
    "import/prefer-default-export": 0,
    "import/no-cycle": 0,
    "no-underscore-dangle": 0,
    "no-console": [2, { "allow": ["warn", "error"] }],
    "no-alert": 2,
    "no-debugger": 2,
    "no-use-before-define": 0,
    "no-param-reassign": [
      2,
      {
        "props": true,
        "ignorePropertyModificationsFor": ["draft", "draftState", "innerDraft"]
      }
    ],
    "prefer-template": 2,
    "require-yield": 0,
    "prettier/prettier": ["error"],
  },
  "settings": {
    "import/resolver": {
      "node": {
        "paths": ["app"]
      },
    }
  },
}
