# SQL Mock Application

### Overview of Application

  This is a SQL mock application to write the Query and visualise the results for it. It has the following features:

  - Resizable `SQL Editor` to write the code
  - `Editor Action Bar` to perform actions like
    - Select predefined datasets and show the corresponding Queries
    - Run Query
    - Kill the running Query
    - Clear Editor's text
    - Switch to a fullscreen view of the Editor
  - The `Table` that displays the results for the Query ran
  - The `Table Status Bar` shows the following details of the executed Query
    - Process ID (Task Id)
    - Status (running | executed | killed)
    - Count of rendered rows out of total rows

### Application URL
https://hello2parth.gitlab.io/sql-mock-app/

### Languages, frameworks, and libraries under the hood

- Used `react`, `react-dom` and `react-scripts`, to run and build SPA with virtual DOM
- `ace-builds` for the `Editor`, which has many capabilities like theming, font-size, and multiple language support. It is widely used and actively maintained by the open source community.
- `react-fluid-table` : It is used to use the `react-table` with the `react-window`.
  -  `react-table` is a headless table library built on top of `react` that provides a wide range of hooks to customise the table.
  -  `react-window` has helped in (virtual table) rendering large numbers of rows by adding and removing the rows in the DOM as the user scrolls.
- `chance` has been used to generate various kinds of random data, like name, age etc., which are required for the table.
- `antd` for general UI components like button, select etc.


### Snapshot of performance measured by `Lighthouse`

![Performance Snapshot](readme_docs/Performance_snapshot.png)
    This has been measured on production build deployed over gitlab pages.

### Optimizations
  - Used `Web Worker` to unblock the main thread and generate a large amount of data.
  - Implemented lazy load for table rows, initially fetching 10000 rows to render, and the rest being fetched lazily after that, so that users don't need to wait for a long time.
  - Used `react-window` for vitual rendering of any number of table rows, so that the browser will not crash for memory over use.
  - Used `useMemo` and `useCallback` to reduce the function execution on every render.
### Available Scripts

#### `npm start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


#### `npm run build`

Builds the app for production to the `build` folder.


