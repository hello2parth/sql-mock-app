import chance from 'chance';

export const Chance = (function _chance() {
  return chance();
})();
