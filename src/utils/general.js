/* eslint-disable prefer-destructuring */
import { STATUS } from '../constants/table';

export const calculateUtils = dataRequest => {
  let showLoader = false;
  let queryMessage = '';
  let executionTime = '';
  let rowCountMessage = '';
  let showError = '';
  let isRunDisabled = false;
  const status = dataRequest.current.status;

  if (status === STATUS.STARTED) {
    queryMessage = 'Query is running';
    showLoader = true;
    isRunDisabled = true;
  } else if (status === STATUS.PARTIAL) {
    rowCountMessage = `Loaded ${dataRequest.current.loaded} Rows out of ${dataRequest.current.total}`;
    queryMessage = 'Query is running';
    isRunDisabled = true;
  } else if (status === STATUS.COMPLETED) {
    queryMessage = 'Query has been successfully executed';
    executionTime = dataRequest.current.executionTime;
    rowCountMessage = `${dataRequest.current.loaded} Rows`;
  } else if (status === STATUS.KILLED) {
    queryMessage = 'Query has been killed';
    rowCountMessage = `Loaded ${dataRequest.current.loaded} Rows out of ${dataRequest.current.total}`;
  } else if (status === STATUS.ERROR) {
    showError = 'Error in fetching data';
  }

  return { showLoader, queryMessage, executionTime, rowCountMessage, showError, isRunDisabled };
};
