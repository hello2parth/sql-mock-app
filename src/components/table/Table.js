import { Table as FluidTable } from 'react-fluid-table';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Spinner from '../spinner';
import styles from './table.module.scss';

function Table({ tableData, isEditorFullScreen, dataRequest, utils }) {
  if (utils.showError) return utils.showError;
  const { data, columns } = tableData;
  return (
    <>
      <div className={classNames('flex flex-grow-1 overflow-auto', { [styles.fullScreen]: isEditorFullScreen })}>
        {utils.showLoader ? <Spinner /> : <FluidTable data={data} columns={columns} className={styles.table} />}
      </div>
      <div className={classNames(styles.resultInfo, { [styles.fullScreen]: isEditorFullScreen })}>
        <div className={styles.tableStatusItem}>Process ID: {dataRequest.current.id}</div>
        {utils.queryMessage && <div className={styles.tableStatusItem}>{utils.queryMessage}</div>}
        {utils.executionTime && <div className={styles.tableStatusItem}>Execution Time: {utils.executionTime}</div>}
        {utils.rowCountMessage && <div className={styles.tableStatusItem}>{utils.rowCountMessage}</div>}
      </div>
    </>
  );
}

Table.defaultProps = {};

Table.propTypes = {
  isEditorFullScreen: PropTypes.bool.isRequired,
  tableData: PropTypes.object.isRequired,
  dataRequest: PropTypes.object.isRequired,
  utils: PropTypes.object.isRequired,
};

export default Table;
