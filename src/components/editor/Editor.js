import { useMemo } from 'react';
import PropTypes from 'prop-types';

import AceEditor from 'react-ace';
import 'ace-builds/src-min-noconflict/ext-searchbox';
import 'ace-builds/src-min-noconflict/ext-language_tools';
import 'ace-builds/src-min-noconflict/mode-sql';
import 'ace-builds/src-min-noconflict/snippets/sql';
import 'ace-builds/src-noconflict/theme-kuroir';

import { Button, Select } from 'antd';
import { getTableOptions } from '../../helpers/table.generator';
import { DEFAULT_TABLE_OPTION } from '../../constants/table';

import styles from './editor.module.scss';
function Editor({
  onRun,
  editorText,
  tableType,
  isEditorFullScreen,
  onKill,
  utils,
  onEditorTextChange,
  onEditorTextClear,
  onTableTypeChange,
  onFullScreen,
}) {
  const tableOptions = useMemo(() => [DEFAULT_TABLE_OPTION, ...getTableOptions()], []);

  return (
    <>
      <AceEditor
        mode="sql"
        theme="kuroir"
        onChange={onEditorTextChange}
        name="sql-ace-editor"
        editorProps={{ $blockScrolling: true }}
        height="150px"
        width="100%"
        className={isEditorFullScreen ? styles.editorContainerFullScreen : styles.editorContainerClass}
        value={editorText}
        fontSize={14}
      />

      <div className={isEditorFullScreen ? styles.editorActionBarFullScreen : styles.editorActionBarClass}>
        <Select value={tableType} className={styles.select} onChange={onTableTypeChange} options={tableOptions} />
        <Button className="ml-8" onClick={onRun} disabled={utils.isRunDisabled}>
          Run
        </Button>
        <Button className="ml-8" onClick={onKill} disabled={!utils.isRunDisabled}>
          Kill
        </Button>
        <Button className="ml-8" onClick={onEditorTextClear} disabled={editorText.length === 0}>
          Clear
        </Button>
        <Button className="ml-8" onClick={onFullScreen}>
          {isEditorFullScreen ? 'Normal Screen' : 'Full Screen'}
        </Button>
      </div>
    </>
  );
}

Editor.defaultProps = {};

Editor.propTypes = {
  onRun: PropTypes.func.isRequired,
  editorText: PropTypes.string.isRequired,
  tableType: PropTypes.string.isRequired,
  onKill: PropTypes.func.isRequired,
  isEditorFullScreen: PropTypes.bool.isRequired,
  utils: PropTypes.object.isRequired,
  onEditorTextChange: PropTypes.func.isRequired,
  onEditorTextClear: PropTypes.func.isRequired,
  onTableTypeChange: PropTypes.func.isRequired,
  onFullScreen: PropTypes.func.isRequired,
};

export default Editor;
