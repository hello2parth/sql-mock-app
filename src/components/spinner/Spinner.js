import styles from './spinner.module.scss';

const Spinner = () => (
  <div className="full-width full-height flex flex-center">
    <div className={styles.spinner}></div>
  </div>
);

Spinner.propTypes = {};

Spinner.defaultProps = {};

export default Spinner;
