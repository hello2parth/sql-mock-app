import { useState, useCallback, useRef } from 'react';
import classNames from 'classnames';

import Editor from './components/editor';
import Table from './components/table';
import TableDataRequests from './helpers/table.generator';
import { DEFAULT_TABLE_OPTION, INITITAL_TABLE_DATA, STATUS, ROWS, TABLE_TYPES_VS_QUERY } from './constants/table';
import { Chance } from './utils/chance';
import { formatTime } from './utils/time';
import { calculateUtils } from './utils/general';

import 'antd/dist/antd.min.css';
import styles from './app.module.scss';

function App() {
  const [isEditorFullScreen, setIsEditorFullScreen] = useState(false);
  const [editorText, setEditorText] = useState('');
  const [tableType, setTableType] = useState(DEFAULT_TABLE_OPTION.value);
  const [tableData, setTableData] = useState(INITITAL_TABLE_DATA);

  const dataRequest = useRef({
    id: 0,
    req: null,
    total: 0,
    loaded: 0,
    executionTime: 0,
    status: STATUS.NA,
  });

  const [utils, setUtils] = useState(() => calculateUtils(dataRequest));

  const getData = useCallback(
    async (id, start, end) => {
      const currentPromise = TableDataRequests.generateTableData({ tableType, id, start, end });
      dataRequest.current.req = currentPromise;
      return currentPromise
        .then(data => {
          if (dataRequest.current.req === currentPromise) {
            return data;
          }
          return [];
        })
        .catch(() => {
          if (dataRequest.current.req === currentPromise) throw new Error();
        });
    },
    [tableType]
  );

  const onRun = useCallback(async () => {
    setIsEditorFullScreen(false);
    setTableData(INITITAL_TABLE_DATA);
    const id = dataRequest.current.id + 1;
    const totalRows = Chance.integer({ min: ROWS.MIN, max: ROWS.MAX });
    dataRequest.current = {
      total: totalRows,
      id,
      loaded: 0,
      executionTime: 0,
      status: STATUS.STARTED,
    };
    setUtils(calculateUtils(dataRequest));
    const startTime = Date.now();
    await getData(id, 0, ROWS.INITIAL)
      .then(data => {
        setTableData(data);
        dataRequest.current.loaded = data.data.length;
        dataRequest.current.status = STATUS.PARTIAL;
        setUtils(calculateUtils(dataRequest));
        return getData(id, data.data.length, totalRows - data.data.length);
      })
      .then(remainingData => {
        setTableData(prevData => ({ data: prevData.data.concat(remainingData.data), columns: prevData.columns }));
        dataRequest.current.loaded = totalRows;
        dataRequest.current.executionTime = formatTime(Date.now() - startTime);
        dataRequest.current.status = STATUS.COMPLETED;
        setUtils(calculateUtils(dataRequest));
      })
      .catch(() => {
        dataRequest.current.status = STATUS.ERROR;
        setUtils(calculateUtils(dataRequest));
      });
  }, [getData]);

  const onKill = useCallback(() => {
    TableDataRequests.cancelGenerateData({ id: dataRequest.current.id });
    dataRequest.current.req = null;
    dataRequest.current.status = STATUS.KILLED;
    setUtils(calculateUtils(dataRequest));
  }, []);

  const onEditorTextChange = useCallback(value => setEditorText(value), []);
  const onEditorTextClear = useCallback(() => setEditorText(''), []);
  const onTableTypeChange = useCallback(value => {
    setTableType(value);
    setEditorText(TABLE_TYPES_VS_QUERY[value]);
  }, []);
  const onFullScreen = useCallback(() => setIsEditorFullScreen(prev => !prev), []);

  return (
    <div className={classNames(styles.app, 'flex', 'flex-column')}>
      <Editor
        {...{
          editorText,
          onRun,
          tableType,
          isEditorFullScreen,
          onKill,
          dataRequest,
          utils,
          onEditorTextChange,
          onEditorTextClear,
          onTableTypeChange,
          onFullScreen,
        }}
      />
      <Table
        {...{
          utils,
          tableData,
          isEditorFullScreen,
          dataRequest,
        }}
      />
    </div>
  );
}

export default App;
