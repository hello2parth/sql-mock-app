import { tableMetaDatas } from './table.metadata';

export const getTableOptions = () => Object.keys(tableMetaDatas).map(v => ({ label: v, value: v }));

const TableDataRequests = (function TableWorkers() {
  const workers = {};
  function cancelGenerateData({ id }) {
    if (id) {
      workers[id].terminate();
    }
  }
  function generateTableData({ tableType, id, start, end }) {
    workers[id] = new Worker(new URL('./table.worker.js', import.meta.url));
    return new Promise((resolve, reject) => {
      workers[id].postMessage({ tableType, start, end });
      workers[id].onmessage = e => {
        const { columns, rowData } = e.data;
        resolve({ columns, data: rowData });
        cancelGenerateData({ id });
      };
      workers[id].onerror = () => {
        reject(new Error('Error in generating table data'));
        cancelGenerateData({ id });
      };
    });
  }
  return { generateTableData, cancelGenerateData };
})();

export default TableDataRequests;
