/* eslint-disable no-restricted-globals */

import { Chance } from '../utils/chance';
import { tableMetaDatas } from './table.metadata';
import { DEFAULT_TABLE_OPTION } from '../constants/table';

const pickRandomTableMetadata = tableType =>
  tableType === DEFAULT_TABLE_OPTION.value
    ? tableMetaDatas[Chance.pickone(Object.keys(tableMetaDatas))]
    : tableMetaDatas[tableType];

const generateColumns = tableMetaData => {
  const columnAccessors = Object.keys(tableMetaData);
  const columns = columnAccessors.map(v => ({
    header: v,
    key: v,
  }));
  return { columnAccessors, columns };
};

const generateRows = (tableMetaData, columnAccessors, start, end) => {
  let columns = [];
  const _end = start + end;
  for (let i = start; i < _end; i += 1) {
    const row = columnAccessors.reduce(
      (accumlator, accessor) => Object.assign(accumlator, { [accessor]: tableMetaData[accessor](i) }),
      {}
    );
    columns = columns.concat(row);
  }
  return columns;
};

self.onmessage = event => {
  const { tableType, start, end } = event.data;
  const tableMetaData = pickRandomTableMetadata(tableType);
  const { columns, columnAccessors } = generateColumns(tableMetaData);
  const rowData = generateRows(tableMetaData, columnAccessors, start, end);
  self.postMessage({ columns, rowData });
};
