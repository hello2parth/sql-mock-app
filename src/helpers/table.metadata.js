import { Chance } from '../utils/chance';

import { TABLE_TYPES } from '../constants/table';

export const tableMetaDatas = {
  [TABLE_TYPES.ORDER_DETAILS]: {
    orderID: id => id + 1,
    productID: () => Chance.integer({ min: 1, max: 100 }),
    unitPrice: () => Chance.dollar(),
    quantity: () => Chance.integer({ min: 1, max: 10 }),
    discount: () => Chance.floating({ min: 0.01, max: 0.99, fixed: 2 }),
  },
  [TABLE_TYPES.SITE_VISITORS]: {
    visitorId: id => id + 1,
    firstName: () => Chance.first(),
    lastName: () => Chance.last(),
    age: () => Chance.age(),
    visits: () => Chance.integer({ min: 1, max: 10 }),
    progress: () => Chance.integer({ min: 1, max: 100 }),
    gender: () => Chance.gender(),
  },
  [TABLE_TYPES.EMPLOYEE]: {
    employeeID: id => id + 1,
    lastName: () => Chance.last(),
    firstName: () => Chance.first(),
    title: () => Chance.profession(),
    titleOfCourtesy: () => Chance.prefix(),
    birthDate: () => Chance.birthday({ string: true }),
    hireDate: () => Chance.date({ string: true }),
    address: () => Chance.address(),
    city: () => Chance.city(),
    region: () => Chance.province({ full: true }),
    postalCode: () => Chance.zip(),
    country: () => Chance.country(),
    homePhone: () => Chance.phone(),
    notes: () => Chance.paragraph({ sentences: 1 }),
  },
};
