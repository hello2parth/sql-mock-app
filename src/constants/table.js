export const INITITAL_TABLE_DATA = {
  columns: [],
  data: [],
};
export const ROWS = {
  INITIAL: 10000,
  MIN: 50000,
  MAX: 100000,
};

export const STATUS = {
  NA: 'NA',
  STARTED: 'STARTED',
  PARTIAL: 'PARTIAL',
  COMPLETED: 'COMPLETED',
  KILLED: 'KILLED',
  ERROR: 'ERROR',
};

export const TABLE_TYPES = {
  RANDOM: 'random',
  ORDER_DETAILS: 'orderDetails',
  SITE_VISITORS: 'siteVisitors',
  EMPLOYEE: 'employee',
};

export const DEFAULT_TABLE_OPTION = { value: TABLE_TYPES.RANDOM, label: TABLE_TYPES.RANDOM };

export const TABLE_TYPES_VS_QUERY = {
  [TABLE_TYPES.RANDOM]: '',
  [TABLE_TYPES.ORDER_DETAILS]: 'SELECT * from northwind..orderDetails',
  [TABLE_TYPES.SITE_VISITORS]: 'SELECT * from northwind..siteVisitors',
  [TABLE_TYPES.EMPLOYEE]: 'SELECT * from northwind..employees',
};
